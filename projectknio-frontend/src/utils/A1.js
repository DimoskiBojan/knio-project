

const parseEmail=(email)=> {
    let content=email.content;
    let invoiceRegex=RegExp('(?<=Број\\s+на\\s+фактура:\\s+)\\d{1,}');
    let userIdRegex= RegExp('(?<=Број\\s+на\\s+претплатник:\\s+)([^\\n]+)');
    let amountRegex=RegExp('(\\d{1,3}\\.)*(\\d{1,3}),\\d{2}(?=\\s+ден\\.)');
    let dueDateRegex=RegExp('(?<=Рок\\s+на\\s+плаќање:\\s+)\\d{2}\\.\\d{2}\\.\\d{4}');
    let forMonthRegex=RegExp('(?<=Сметка\\s+за\\s+месец:\\s+)\\d{2}\\/\\d{4}');
    let phoneNumberRegex=RegExp('(?<=Телефонски\\s+број:\\s+)\\w{11,12}');
    let tariffRegex=RegExp('(?<=Тарифен\\s+модел:\\s+)([^\\n]+)');


    let invoice=content.match(invoiceRegex);
    let userId=content.match(userIdRegex);
    let amount=content.match(amountRegex);
    let dueDate=content.match(dueDateRegex);
    let forMonth=content.match(forMonthRegex);
    let phoneNumber=content.match(phoneNumberRegex);
    let tariff=content.match(tariffRegex);


    if (invoice != null && userId != null && amount != null && dueDate !=null && forMonth != null)
    {
        console.log(phoneNumber);
        return {
            "invoice": invoice[0],
            "userId": userId[0],
            "amount": amount[0],
            "dueDate": dueDate[0],
            "forMonth": forMonth[0],
            "phoneNumber": phoneNumber != null ? phoneNumber[0] : "Недостапно",
            "tariff": tariff != null ? tariff[0] : "Недостапно",
            "type": "A1"
        }
    }
    return null;

};

export default parseEmail;