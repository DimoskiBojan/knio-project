

const parseEmail=(email)=> {
    let content=email.content;
    ///Regular expresions
    let invoiceRegex=RegExp('((?<=број\\s+)\\d{1,})(?=\\s+и\\s+рок)');
    let userIdRegex= RegExp('(?<=број\\s+на\\s+претплатник\\s+)\\d{1,}');
    let amountRegex=RegExp('\\d{1,}\\.\\d{2}(?=\\s+денари\\.)');
    let dueDateRegex=RegExp('(?<=рок\\s+на\\s+плаќање\\s+)\\d{2}\\.\\d{2}\\.\\d{4}');
    let forMonthRegex=RegExp('(?<=месец\\s+)\\d{1,2}-\\d{4}');

    ///parsed content
    let invoice=content.match(invoiceRegex);
    let userId=content.match(userIdRegex);
    let amount=content.match(amountRegex);
    let dueDate=content.match(dueDateRegex);
    let forMonth=content.match(forMonthRegex);
    


    if (invoice != null && userId != null && amount != null && dueDate !=null && forMonth != null )
    {
        return {
            "invoice": invoice[0],
            "userId": userId[0],
            "amount": amount[0],
            "dueDate":dueDate[0],
            "forMonth":forMonth[0],
            "type": "VODOVOD"
        }
    }
    return null;

};

export default parseEmail;