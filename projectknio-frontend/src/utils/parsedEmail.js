import evnParseEmail from "./EVN";
import a1ParseEmail from "./A1";
import vodovodParseEmail from "./Vodovod";

const parsedcontent=(email)=>{
    let newcontent={};

    if(email.type==="EVN")
    {
        newcontent=evnParseEmail(email);
    }
    if(email.type==="A1")
    {
        newcontent=a1ParseEmail(email);
    }
    if(email.type==="VODOVOD")
    {
        newcontent= vodovodParseEmail(email);
    }

    return newcontent;

}

export default parsedcontent;