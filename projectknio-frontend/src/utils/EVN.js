

const parseEmail=(email)=> {

    ///invoice dve mi naogja
    let content=email.content;
    let invoiceRegex=RegExp('(?<=фактура\\s+за\\s+електрична\\s+енергија\\s+)([^\\s]+)');
    let userIdRegex= RegExp('(?<=број\\s+на\\s+корисник\\s+)\\d{1,}');
    let amountRegex=RegExp('\\d{1,}\\.\\d{2}(?=\\s+денари)');
    let dueDateRegex=RegExp('(?<=рок\\s+на\\s+плаќање\\s+до\\s+)\\d{2}\\.\\d{2}\\.\\d{4}');


    let invoice=content.match(invoiceRegex);
    let userId=content.match(userIdRegex);
    let amount=content.match(amountRegex);
    let dueDate=content.match(dueDateRegex);

    if (invoice != null && userId != null && amount != null && dueDate !=null )
    {
        return {
            "invoice": invoice[0],
            "userId": userId[0],
            "amount": amount[0],
            "dueDate":dueDate[0],
            "type": "EVN"
        }
    }
    return null;

};


export default parseEmail;