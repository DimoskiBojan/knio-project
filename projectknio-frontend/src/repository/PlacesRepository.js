import axios from '../custom-axios/axios'
import { isAuthenticated } from "./UserRepository";

const PlacesService = {
    fetchPlaces: () => {
        return axios.get("/places")
    },
    fetchPlaceRatings: (placeId) => {
        return axios.get(`/places/${placeId}/ratings`);
    },
    addPlaceRating: (placeId, user, rating) => {
        let loggedInUser;
        if(isAuthenticated()){
            let jwt = require("jsonwebtoken");
            let token = localStorage.getItem("x-access-token").replace('Bearer ','');
            let decodedToken = jwt.decode(token);
            loggedInUser=decodedToken.sub;

            return axios.post("/places/${placeId}/ratings", rating, {
                headers: {
                    'User': loggedInUser
                }
            });
        }

    }

};

export default PlacesService;