import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect, Link} from "react-router-dom";
import './App.css';
import placesService from "../../repository/PlacesRepository"
import Header from "../Header/header";
import TakeDataFromEmail from "../Email/TakeDataFromEmailForm/TakeDataFromEmail";
import ShowParsedEmailInfo from "../Email/ShowParsedEmailInfo/ShowParsedEmailInfo";
import LogIn from "../LogIn/logIn";
import Home from "../Home/home";
import Register from "../Register/register";
import NewParsedEmails from "../User/newParsedEmails";
import TextToSpeech from "../TextToSpeech/textToSpeech";
import Places from "../Places/PlacesList/placesList";
import PlaceRatings from "../Places/PlacesRatings/placesRatings";

class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      emaildata: [],
      places: []
    };
  }

  componentDidMount() {
    this.loadPlaces();
  };

  loadPlaces = () => {
        placesService.fetchPlaces().then(response => {
          this.setState({"places": response.data})
        });
  };

  addPlaceRating = (placeId, user, rating) => {

  };

  render(){
    return (
        <Router>
          <Header/>
          <main role="main" className="mt-3">
            <div className="container">
              <Switch>
                <Route path={"/parse-data"}>
                  <TakeDataFromEmail onSubmitt={(newEmail) => this.setState({emaildata: newEmail})}/>
                </Route>
                <Route path={"/parsed-data"}>
                  <ShowParsedEmailInfo email={this.state.emaildata}/>
                </Route>
                <Route path={"/text-to-speech"}>
                  <TextToSpeech/>
                </Route>
                <Route path={"/places/:placeId/ratings"}>
                  <PlaceRatings onAdd={this.addPlaceRating}/>
                </Route>
                <Route path={"/places"}>
                  <Places places={this.state.places}/>
                </Route>
                <Route path={"/login"}>
                  <LogIn/>
                </Route>
                <Route path={"/register"}>
                  <Register/>
                </Route>
                <Route path={"/newbills"}>
                  <NewParsedEmails/>
                </Route>
                <Route path={"/"}>
                  <Home/>
                </Route>
              </Switch>
            </div>
          </main>
        </Router>
    );
  }

}

export default App;