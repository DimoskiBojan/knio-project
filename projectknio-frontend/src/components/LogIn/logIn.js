import React, {useState} from "react";
import { login } from '../../repository/UserRepository';
import {Link} from "react-router-dom";


const LogIn = (props) => {
    const [userInfo, setUserInfo] = useState({username: '', password: ''});

    const handleInputChange = (e) => {
        setUserInfo({
            ...userInfo,
            [e.target.name]: e.target.value
        });
    }

    const submitLogin = (e) => {
        e.preventDefault();
        login(userInfo)
            .then(token => window.location = '/')
            .catch(err => alert(err));
    }

    return (
        <div className="row text-center justify-content-center mt-5">
            <form onSubmit={submitLogin} className="form-signin bg-light p-5">
                <img className="w-75 mb-3" src={require('../../assets/User_Circle.png')}/>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input name="username" id="inputEmail" className="form-control" placeholder="Email address" required=""
                       autoFocus="" onChange={handleInputChange}/>
                <label htmlFor="inputPassword" className="sr-only">Password</label>
                <br/>
                <input name="password" type="password" id="inputPassword" className="form-control" placeholder="Password"
                       required="" onChange={handleInputChange}/>
                <small>Доколку немаш профил, регистрирај се <Link to={"/register"}>тука</Link> </small>

                <button className="btn btn-lg btn-success btn-block mt-3" type="submit">Најави се</button>
            </form>
        </div>
    )
};

export default LogIn;