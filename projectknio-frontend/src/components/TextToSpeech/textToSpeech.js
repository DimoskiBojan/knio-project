import React, {useState} from "react";

const TextToSpeech = (props) => {
    const responsiveVoice = window.responsiveVoice;
    const [text, setText] = useState("");

    const handleInputChange = (e) => {
        setText(e.target.value);
    }

    const onPlay = (e) => {
        responsiveVoice.speak(text, "Macedonian Male", {rate: 0.8});
    }

    return (
        <div className="mt-5">
            <h3 className="text-upper text-left text-info">Текст во говор</h3>
            <h4 className="mb-5 text-secondary">Копирајте го текстот кој сакате да биде прочитан</h4>

            <div className="form-group">
                <label htmlFor="textcontent">Вашиот текст</label>
                <textarea
                    className="form-control"
                    id="textcontent"
                    rows="10"
                    onChange={handleInputChange}
                    name={"textcontent"}
                    required
                ></textarea>
            </div>
            <div
                className="text-center">
                <button
                    className="btn btn-success text-upper float-right"
                    onClick={onPlay}
                >
                    Прочитај
                </button>
            </div>
        </div>
    )
};

export default TextToSpeech;
