import React from "react";
import {Link} from "react-router-dom";
import {isAuthenticated, logOut} from "../../repository/UserRepository";


const Header = ()=> {
    return(
        <header>
            <nav className="navbar navbar-expand-md navbar-dark navbar-fixed bg-success">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse text-white" id="navbarCollapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to={"/"}>Почетна</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={"/parse-data"}>Парсирај e-mail</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={"/text-to-speech"}>Текст-во-говор</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={"/places"}>Места</Link>
                        </li>
                    </ul>
                    {
                        (isAuthenticated()) ?
                            (<form className="form-inline mt-2 mt-md-0 ml-3">
                                <Link to={"/newbills"}> <span className="text-white mr-3">Нови Сметки</span></Link>
                                <a href="/login" onClick={logOut}><div className="btn my-2 my-sm-0 btn-outline-light">Одјави се</div></a>
                            </form>) :
                            (<form className="form-inline mt-2 mt-md-0 ml-3">
                                <Link to={"/login"}><div className="btn my-2 my-sm-0 btn-outline-light">Најави се</div></Link>
                            </form>)
                    }
                </div>
            </nav>
        </header>
    );

};

export default Header;