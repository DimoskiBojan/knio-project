import React, {useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom";
import parseEmail from "../../../utils/parsedEmail"
import Header from "../../Header/header";
import ShowA1ParsedEmail from "./InterfaceForShowingParsedEmail/showA1ParsedEmail";
import ShowEVNParsedEmail from "./InterfaceForShowingParsedEmail/showEVNParsedEmail";
import ShowVODOVODParsedEmail from "./InterfaceForShowingParsedEmail/showVODOVODParsedEmail";
import ParseFail from "./InterfaceForShowingParsedEmail/parseFail";



const ShowParsedEmailInfo =(props)=>{
    let generatedInfo = parseEmail(props.email);
    // let history = useHistory();
    // history.push("/parsed-data");
    console.log(generatedInfo);
    if(generatedInfo==null)
    {
        return (<div><ParseFail/></div>)
    }
    else if(generatedInfo.type === "A1")
    {
        return (
        <div>
            <ShowA1ParsedEmail info={generatedInfo}/>
        </div>
        );
    }
    else if (generatedInfo.type === "EVN")
    {
        return (
            <div>
                <ShowEVNParsedEmail info={generatedInfo}/>
            </div>
        );
    }
    else if(generatedInfo.type === "VODOVOD")
    {
        return (
            <div>
                <ShowVODOVODParsedEmail info={generatedInfo}/>
            </div>
        );
    }

    return ("No generated info!");

};

export default ShowParsedEmailInfo;