import React from "react";

const ParseFail =(props)=>{
    const buttonStyle = {
        cursor: 'default'
    };
    return(
        <div >
            <button className="btn btn-lg btn-danger" style={buttonStyle} disabled>Неуспешно</button>
            <br></br>
            <p>Парсирањето не успеа. Ве молиме проверете ги внесените податоци дали се точни.</p>
        </div>
    );

};

export default ParseFail;