import React from "react";

const ShowA1ParsedEmail =(props)=>{
    const buttonStyle = {
        cursor: 'default'
    };

    return(
        <div className="container">
            <div className="row">
                <div className="col-xs-12 col-md-6 float-left">
                    <img  src={require('../../../../assets/logo@2x.png')}/>
                </div>

                <div className="col-xs-12 col-md-6">
                    <div className="well well-sm">
                        <div className="row">
                            <button className="btn btn-lg btn-success mb-3 h-25" style={buttonStyle} disabled>Успешно</button>
                            <p className="w-100">
                                <span className="font-weight-bold"> Број на фактура</span> : {props.info.invoice}
                            <br/>
                                <span className="font-weight-bold"> Број на корисник</span> : {props.info.userId}
                            <br/>
                                <span className="font-weight-bold"> Сума</span> : {props.info.amount}
                            <br/>
                                <span className="font-weight-bold"> Рок на плаќање</span> : {props.info.dueDate}
                            <br/>
                                <span className="font-weight-bold"> За месец </span>: {props.info.forMonth}
                            <br/>
                                <span className="font-weight-bold"> За телефонски број</span> : {props.info.phoneNumber}
                            <br/>
                                <span className="font-weight-bold"> Тарифа </span>: {props.info.tariff}
                            <br/>
                                <span className="font-weight-bold"> Тип</span> :  {props.info.type}
                            <br/>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

};

export default ShowA1ParsedEmail;