import React, {useEffect, useState} from "react";
import {Link, Redirect, useHistory} from "react-router-dom";



const TakeDataFromEmail=(props)=>{
    const history=useHistory();
    const [billType,setBillType]=useState("");
    const [emailContent, setEmailContent]=useState("");
    const handleChangeBillType = (e) => {
        setBillType(e.target.value);
    };
    const handleChangeEmailContent = (e) => {
        setEmailContent(e.target.value);
    };
    const handleSubmit=(event)=>
    {
        event.preventDefault();
        const newEmail={
            "type":billType,
            "content":emailContent
        };

        props.onSubmitt(newEmail);
        history.push("/parsed-data")

    };

    return(
        <div className="mt-5">
            <h3 className="text-upper text-left text-info">Парсирање на емаил</h3>
            <h4 className="mb-5 text-secondary">Копирајте ја содржината на емаилот за сметката и изберете го типот на сметката</h4>

            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <select
                        className="form-control"
                        id="billchoice"
                        required
                        onChange={handleChangeBillType}
                        name="billchoice"
                    >
                        <option value="" selected disabled hidden>Избери тип на сметка</option>
                        <option value="A1">A1</option>
                        <option value="EVN">EVN</option>
                        <option value="VODOVOD">Водовод</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="emailcontent">Содржина на e-mail</label>
                    <textarea
                        className="form-control"
                        id="emailcontent"
                        rows="10"
                        onChange={handleChangeEmailContent}
                        name={"emailcontent"}
                        required
                    ></textarea>
                </div>
                <div
                    className="text-center">
                        <button
                            className="btn btn-success text-upper float-right"
                        >
                            Внеси сметка
                        </button>
                </div>
            </form>
        </div>
    );

}

export default TakeDataFromEmail;