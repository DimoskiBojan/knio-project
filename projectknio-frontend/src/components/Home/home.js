import React from "react";
import "./home.css";
import {isAuthenticated} from "../../repository/UserRepository";

const Home = () => {
    let user;
    if(isAuthenticated()){
        let jwt = require("jsonwebtoken");
        let token = localStorage.getItem("x-access-token").replace('Bearer ','');
        let decodedToken = jwt.decode(token);
        user=decodedToken.sub;
    }

    return(
        <div>
            <div className="row mt-5">
                <div className={"col-12 text-center text-info"}>
                    <h1 className="font-weight-bold">Проект пристапност</h1>
                </div>
            </div>
            <div className="row mb-5 text-">
                <div className={"col-12 text-center text-secondary"}>
                    <h2>Искористете ги нашите алатки за да ви помогнат во пристапноста</h2>
                </div>
            </div>
            <hr/>
            <div className="row">
                <div className="col-md-4 mb-2">
                    <a href="/places" className="btn btn-success btn-lg btn-block btn-huge font-weight-bold"><i className="fa fa-map-marker fa-5x"
                                                                                                          aria-hidden="true"></i> Места</a>
                </div>
                <div className="col-md-4 mb-2">
                    <a href="/parse-data" className="btn btn-success btn-lg btn-block btn-huge font-weight-bold"><i className="fa fa-envelope fa-5x"
                                                                                         aria-hidden="true"></i> Парсирај емаил</a>
                </div>
                <div className="col-md-4">
                    <a href="/text-to-speech" className="btn btn-success btn-lg btn-block btn-huge font-weight-bold"><i className="fa fa-microphone fa-5x"
                                                                                         aria-hidden="true"></i> Текст во говор</a>
                </div>
            </div>
            <hr/>
        </div>

    );

};

export default Home;
