import React, {useEffect} from "react";
import PlacesListItem from "../PlacesListItem/placesListItem";

const Places = (props) => {

    if(props.places.length === 0){
        return (
            <div className="row">
                <h4>Моментално нема места за приказ.</h4>
            </div>
        )
    }
    return (
        <div className="mt-5">
            <h3 className="text-upper text-left text-info">Места</h3>
            <h4 className="mb-5 text-secondary">Разгледајте места и видете ги нивните оцени за пристапност</h4>

            <div className="row">
                {
                    props.places.map(place => (
                    <PlacesListItem key={place.id} place={place} />
                ))}
            </div>
        </div>
    )
};

export default Places;