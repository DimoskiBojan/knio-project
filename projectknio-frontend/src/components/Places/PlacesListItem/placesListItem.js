import React, {useEffect} from "react";
import "./placesListItem.css";

const PlacesListItem = (props) => {

    return (
        <div className="col-lg-4 col-md-6 col-sm-6 mb-4">
            <article className="card">
                <header className="title-header">
                    <h3>{props.place.name}</h3>
                </header>
                <div className="card-block">
                    <div className="img-card">
                        <img src={props.place.photoURL} alt="Place" className="w-100"/>
                    </div>
                    <div className="tagline card-text text-xs-center">
                        <ul>
                            <li>Адреса: {props.place.address}</li>
                            <li>Телефон: {props.place.phone}</li>
                            <li>Работно време: {props.place.open_from} - {props.place.open_to}</li>
                        </ul>
                    </div>
                    <div className="text-center">
                        <a href={`places/${props.place.id}/ratings`} className="btn btn-success  mr-sm-0 mr-lg-5 mb-4">Рејтинзи</a>
                        <a href={props.place.gmapsURL} target="_blank" className="btn btn-success mb-4">Прикажи на мапа</a>
                    </div>
                </div>
            </article>
        </div>
    )
};

export default PlacesListItem;