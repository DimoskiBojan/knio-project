import React, {useEffect, useState} from "react";
import {useParams, Link} from 'react-router-dom';
import axios from '../../../custom-axios/axios'
import "./placesRatings.css";
import PlacesListItem from "../PlacesListItem/placesListItem";
import PlaceRatingsItem from "./PlacesRatingItem/placesRatingItem";

const PlaceRatings = (props) => {
    const [ratings, setRatings] = useState([]);
    const [newRating, setNewRating] = useState({});
    const {placeId} = useParams();

    useEffect(() => {
            axios.get(`/places/${placeId}/ratings`).then((response) => {
                setRatings(response.data);
            });
    }, []);

    const handleAddRating = (e) => {
        e.preventDefault();
    }

    return (
        <div>
            <div>
                <Link to={"/places"} className="btn btn-sm btn-info mb-3">Назад кон Места</Link>
                <h3 className="text-upper text-left text-info">Рејтинзи и прегледи</h3>
                <h4 className="mb-4 text-secondary">Видете ги мислењата на корисниците за пристапноста на местото</h4>
            </div>
            {(ratings.length === 0) ? (
                <div className="mb-5">
                    <h5>Моментално нема рејтинзи.</h5>
                </div>
                ) : ratings.map(rating => (
                <PlaceRatingsItem key={rating.id} rating={rating} />
            ))
            }
            <div className="bg-white rounded shadow-sm p-4 mb-5 rating-review-select-page">

                <form onSubmit={handleAddRating}>
                    <h5 className="mb-4">Дадете рејтинг</h5>
                    <div className="mb-4">
                        <span className="star-rating">
                              <i className="fa fa-star fa-2x active"></i>
                              <i className="fa fa-star fa-2x active"></i>
                              <i className="fa fa-star fa-2x active"></i>
                              <i className="fa fa-star fa-2x active"></i>
                              <i className="fa fa-star fa-2x"></i>
                        </span>
                    </div>
                    <div className="form-group">
                        <label>Ваш коментар</label>
                        <textarea className="form-control" rows="3"></textarea>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-success" type="submit">Прати</button>
                    </div>
                </form>
            </div>

        </div>
    )
};

export default PlaceRatings;