import React from "react";

import "../placesRatings.css";

const PlaceRatingsItem = (props) => {
    var stars = [];

    for (var i = 0; i < props.rating.rating; i++) {
        stars.push(<i className="fa fa-star"></i>);
    }
    if(i < 5) {
        while(i <= 4){
            stars.push(<i className="fa fa-star-o"></i>);
            i++;
        }
    }

    return (
        <div className="bg-white rounded shadow-sm p-4 mb-4 restaurant-detailed-ratings-and-reviews">
            <div className="reviews-members pt-4 pb-4">
                <div className="media">
                    <img alt="Generic placeholder image"
                         src="http://bootdey.com/img/Content/avatar/avatar6.png"
                         className="mr-3 rounded-pill"/>
                    <div className="media-body">
                        <div className="reviews-members-header">
                            <h6 className="mb-1"><a className="text-black" href="#">{props.rating.user.username}</a></h6>
                            <p className="text-gray">
                                <span className="star-rating">
                                      {stars}
                                </span>
                            </p>
                        </div>
                        <div className="reviews-members-body">
                            <p>{props.rating.comment}</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
        </div>
    )
};

export default PlaceRatingsItem;