import axios from 'axios';
import {isAuthenticated} from "../repository/UserRepository";

const instance = axios.create({
    baseURL: 'http://localhost:8080',
    headers: {
        'Access-Control-Allow-Origin': '*'
    },
});
if(isAuthenticated()){
    instance.interceptors.request.use(function (config) {
        const token = localStorage.getItem("x-access-token");
        config.headers.Authorization = token;

        return config;
    });
}

export default instance;