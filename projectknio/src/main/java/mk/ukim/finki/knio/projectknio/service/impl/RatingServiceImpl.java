package mk.ukim.finki.knio.projectknio.service.impl;

import mk.ukim.finki.knio.projectknio.model.Rating;
import mk.ukim.finki.knio.projectknio.repository.RatingRepository;
import mk.ukim.finki.knio.projectknio.service.PlaceService;
import mk.ukim.finki.knio.projectknio.service.RatingService;
import mk.ukim.finki.knio.projectknio.user.ApplicationUserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;
    private final PlaceService placeService;
    private final ApplicationUserRepository applicationUserRepository;

    public RatingServiceImpl(RatingRepository ratingRepository, PlaceService placeService, ApplicationUserRepository applicationUserRepository) {
        this.ratingRepository = ratingRepository;
        this.placeService = placeService;
        this.applicationUserRepository = applicationUserRepository;
    }

    @Override
    public List<Rating> getPlaceRatings(Integer placeId) {
        return this.ratingRepository.findByPlace_IdEquals(placeId);
    }

    @Override
    public Rating addPlaceRating(Integer placeId, String user, Rating rating) {
        rating.setPlace(placeService.getPlaceById(placeId));
        rating.setUser(applicationUserRepository.findByUsername(user));
        return this.ratingRepository.save(rating);
    }
}
