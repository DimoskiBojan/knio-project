package mk.ukim.finki.knio.projectknio.service;

import mk.ukim.finki.knio.projectknio.model.Place;

import java.util.List;

public interface PlaceService {
    List<Place> getAllPlaces();

    Place getPlaceById(Integer id);

    Place addPlace(Place place);
}
