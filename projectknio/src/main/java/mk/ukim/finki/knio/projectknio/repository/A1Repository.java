package mk.ukim.finki.knio.projectknio.repository;

import mk.ukim.finki.knio.projectknio.model.A1;
import org.springframework.data.jpa.repository.JpaRepository;

public interface A1Repository extends JpaRepository<A1, String> {
}
