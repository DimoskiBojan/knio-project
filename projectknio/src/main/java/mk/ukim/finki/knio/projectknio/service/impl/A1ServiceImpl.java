package mk.ukim.finki.knio.projectknio.service.impl;

import mk.ukim.finki.knio.projectknio.model.A1;
import mk.ukim.finki.knio.projectknio.service.A1Service;
import mk.ukim.finki.knio.projectknio.util.A1RegularExpressions;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class A1ServiceImpl implements A1Service {

    @Override
    public A1 parseEmail(String content) {
        List<Pattern> patterns = A1RegularExpressions.getPatterns();
        List<String> parsedData = new ArrayList<>();

        patterns.forEach(pattern->{
            Matcher matcher=pattern.matcher(content);
            parsedData.add((matcher.find() ? content.substring(matcher.start(), matcher.end()) : ""));
        });
        if(!parsedData.isEmpty()) {
            String invoce = parsedData.get(0);
            String userId = parsedData.get(1);
            Double amount= Double.parseDouble(parsedData.get(2).substring(1, parsedData.get(2).length() - 3)); // amout.amout,00
//            YearMonth forMonth = YearMonth.parse(parsedData.get(3));// mm/yyyy
//            LocalDate dueDate = LocalDate.parse(parsedData.get(4));// dd.mm.yyyy
            String phoneNumber= parsedData.get(5);
            String tariff = parsedData.get(6);
            LocalDate dateParsed= LocalDate.now();

            if(!invoce.equals("") && !userId.equals("") && amount!=0  )
            {
                A1 a1= new A1(invoce,userId, amount, YearMonth.now(), LocalDate.now(), phoneNumber, tariff, dateParsed, "Uspeshen", content);
                a1.toString();
                return a1;
            }
        }

        return null;
    }
}
