/*
package mk.ukim.finki.knio.projectknio.util;

import mk.ukim.finki.knio.projectknio.model.Place;
import mk.ukim.finki.knio.projectknio.model.Rating;
import mk.ukim.finki.knio.projectknio.service.PlaceService;
import mk.ukim.finki.knio.projectknio.service.RatingService;
import mk.ukim.finki.knio.projectknio.user.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    @Autowired
    private PlaceService placeService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Override
    public void run(String...args) throws Exception {

        Place place = new Place();
        place.setName("Скопје Сити Мол");
        place.setAddress("Љубљанска 4, Скопје 1000");
        place.setPhone("+38923217012");
        place.setOpen_from(LocalTime.of(8,0));
        place.setOpen_to(LocalTime.of(22, 0));
        place.setPhotoURL("https://lh5.googleusercontent.com/p/AF1QipN8kXm1xmHBzjMAZ_qHMCsSvkxXQouJcUh3eg88=w408-h306-k-no");
        place.setGmapsURL("https://www.google.com/maps/place/Skopje+City+Mall/@42.0030619,21.3915079,16z/data=!4m5!3m4!1s0x0:0x7741a9cfe3c98a55!8m2!3d42.0042189!4d21.3917112");

        Place newPlace = placeService.addPlace(place);

        Rating rating = new Rating();
        rating.setPlace(newPlace);
        rating.setUser(applicationUserRepository.findByUsername("bojan.dimoski@students.finki.ukim.mk"));
        rating.setRating(4);
        rating.setComment("Молот е пристапен, има повеќе влезови, има лифтови. Супер место!");

        ratingService.addPlaceRating(newPlace.getId(), "bojan.dimoski@students.finki.ukim.mk", rating);

        place = new Place();
        place.setName("Силбо");
        place.setAddress("Улица Мајаковски, Скопје 1000");
        place.setPhone("+38971333529");
        place.setOpen_from(LocalTime.of(0,0));
        place.setOpen_to(LocalTime.of(0, 0));
        place.setPhotoURL("https://lh5.googleusercontent.com/p/AF1QipPL4GDDEHpGca91_gphoy1ESzfu2ddntgfiVBci=w408-h272-k-no");
        place.setGmapsURL("https://www.google.com/maps/place/Silbo/@42.0016613,21.4117372,16.25z/data=!4m12!1m6!3m5!1s0x1354146e17dd809b:0x7741a9cfe3c98a55!2sSkopje+City+Mall!8m2!3d42.0042193!4d21.3917118!3m4!1s0x1354144f3ee1703f:0x8d5729b2bc708377!8m2!3d42.0001089!4d21.4174819");

        placeService.addPlace(place);

        place = new Place();
        place.setName("Драмски театар Скопје");
        place.setAddress("Бранислав Нушиќ 4, Скопје 1000");
        place.setPhone("+38923063453");
        place.setOpen_from(LocalTime.of(10,0));
        place.setOpen_to(LocalTime.of(23, 0));
        place.setPhotoURL("https://lh5.googleusercontent.com/p/AF1QipP3B871Xwjm7RGcrD0Hp0Y0KPxL0WDFrVto15kJ=w426-h240-k-no");
        place.setGmapsURL("https://www.google.com/maps/place/Drama+Theater+Skopje/@42.0011464,21.409382,16.25z/data=!4m12!1m6!3m5!1s0x1354146e17dd809b:0x7741a9cfe3c98a55!2sSkopje+City+Mall!8m2!3d42.0042193!4d21.3917118!3m4!1s0x13541443c0a611b1:0x29108fba193d295d!8m2!3d42.0023263!4d21.4077294");

        placeService.addPlace(place);

        place = new Place();
        place.setName("Фитнес Хаус");
        place.setAddress("Булевар Партизански Одреди 40, Скопје 1000");
        place.setPhone("+38972699999");
        place.setOpen_from(LocalTime.of(8,0));
        place.setOpen_to(LocalTime.of(1, 0));
        place.setPhotoURL("https://lh5.googleusercontent.com/p/AF1QipNovv-mEXL8cKN2gUmS3JqniNl6WgHILDsD6KS1=w426-h240-k-no");
        place.setGmapsURL("https://www.google.com/maps/place/Fitness+House/@42.0023971,21.4148221,16.42z/data=!4m5!3m4!1s0x13541445ede50753:0x73006fac30b1ab07!8m2!3d42.0005863!4d21.4147165");

        placeService.addPlace(place);

        place = new Place();
        place.setName("Скопски Мерак");
        place.setAddress("улица Дебарца 51, Скопје 1000");
        place.setPhone("+38978343630");
        place.setOpen_from(LocalTime.of(8,0));
        place.setOpen_to(LocalTime.of(1, 0));
        place.setPhotoURL("https://lh5.googleusercontent.com/p/AF1QipPYkjLcmtqeZjSx4HikngP5hU9Iux9oCixI0RPO=w408-h306-k-no");
        place.setGmapsURL("https://www.google.com/maps/place/Skopski+Merak/@42.0020487,21.4107003,16.67z/data=!4m5!3m4!1s0x1354144e523418ef:0xd97e6a60f538edf0!8m2!3d42.0010048!4d21.4199254");

        placeService.addPlace(place);
    }
}*/
