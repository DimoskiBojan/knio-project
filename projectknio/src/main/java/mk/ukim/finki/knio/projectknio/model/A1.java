package mk.ukim.finki.knio.projectknio.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.YearMonth;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table
public class A1 {
    @Id
    private String invoce;
    private String userId;
    private Double amount; // amount.amount,00
    private YearMonth forMonth;// mm/yyyy
    private LocalDate dueDate;// dd.mm.yyyy
    private String phoneNumber;
    private String tariff;
    //    private String email;
    private LocalDate dateParsed;
    private String status;
    private String content;

    public String toString()
    {
        System.out.println("hello");
        System.out.println(invoce);
        System.out.println(userId);
        System.out.println(amount);
        System.out.println(forMonth);
        System.out.println(dueDate);
        System.out.println(phoneNumber);
        System.out.println(tariff);
        System.out.println(dateParsed);
        System.out.println(status);
        System.out.println(content);
        return null;
    }

}