package mk.ukim.finki.knio.projectknio.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@Entity
@Table
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String address;
    private String phone;
    private LocalTime open_from;
    private LocalTime open_to;
    @Column(length = 1024)
    private String photoURL;
    @Column(length = 1024)
    private String gmapsURL;

    @JsonIgnore
    @OneToMany(mappedBy = "place")
    private List<Rating> ratings;

    public Place() {
        this.ratings = new ArrayList<>();
    }
}
