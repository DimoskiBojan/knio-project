package mk.ukim.finki.knio.projectknio.service.impl;

import mk.ukim.finki.knio.projectknio.model.Place;
import mk.ukim.finki.knio.projectknio.model.exceptions.PlaceNotFoundException;
import mk.ukim.finki.knio.projectknio.repository.PlaceRepository;
import mk.ukim.finki.knio.projectknio.service.PlaceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaceServiceImpl implements PlaceService {

    private final PlaceRepository placeRepository;

    public PlaceServiceImpl(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Override
    public List<Place> getAllPlaces() {
        return this.placeRepository.findAll();
    }

    @Override
    public Place getPlaceById(Integer id) {
        return this.placeRepository.findById(id).orElseThrow(PlaceNotFoundException::new);
    }

    @Override
    public Place addPlace(Place place) {
        return this.placeRepository.save(place);
    }
}
