package mk.ukim.finki.knio.projectknio.web.rest;

import mk.ukim.finki.knio.projectknio.model.Place;
import mk.ukim.finki.knio.projectknio.model.Rating;
import mk.ukim.finki.knio.projectknio.service.PlaceService;
import mk.ukim.finki.knio.projectknio.service.RatingService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/places")
@CrossOrigin(origins = "http://localhost:3000")
public class PlacesController {

    private final PlaceService placeService;
    private final RatingService ratingService;

    public PlacesController(PlaceService placeService, RatingService ratingService) {
        this.placeService = placeService;
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<Place> getAllPlaces() {
        return this.placeService.getAllPlaces();
    }

    @PostMapping
    public Place addPlace(@RequestBody Place place) {
        return this.placeService.addPlace(place);
    }

    @GetMapping(path = "/{id}/ratings")
    public List<Rating> getPlaceRatings(@PathVariable("id") Integer id) {
        return this.ratingService.getPlaceRatings(id);
    }

    @PostMapping(path = "/{id}/ratings")
    public Rating addPlaceRating(@PathVariable("id") Integer id, @RequestHeader("User") String user, @RequestBody Rating rating) {
        return this.ratingService.addPlaceRating(id, user, rating);
    }

}