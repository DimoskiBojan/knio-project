package mk.ukim.finki.knio.projectknio.util;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class A1RegularExpressions {

    private A1RegularExpressions(){
        throw new UnsupportedOperationException();
    }

    public static List<Pattern> getPatterns() {
        return Arrays.asList(
                Pattern.compile("(?<=Број\\\\sна\\\\sфактура:\\\\s)\\\\d{1,}"),
                Pattern.compile("(?<=Тарифен\\\\sмодел:\\\\s)([^\\\\n]+)"),
                Pattern.compile("(?<=Телефонски\\sброј:\\s)\\w{11,12}"),
                Pattern.compile("(?<=Рок\\sна\\sплаќање:\\s)\\d{2}\\.\\d{2}\\.\\d{4}"),
                Pattern.compile("(?<=Сметка\\sза\\sмесец:\\s)\\d{2}\\/\\d{4}"),
                Pattern.compile("(\\d{1,3}\\.)*(\\d{1,3}),\\d{2}(?=\\s+ден\\.)"),
                Pattern.compile("(?<=Број\\sна\\sпретплатник:\\s)([^\\n]+)")
        );
    }

}
