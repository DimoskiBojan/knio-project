package mk.ukim.finki.knio.projectknio.repository;

import mk.ukim.finki.knio.projectknio.model.Place;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place, Integer> {
}
