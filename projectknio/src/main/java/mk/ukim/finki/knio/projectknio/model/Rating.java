package mk.ukim.finki.knio.projectknio.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mk.ukim.finki.knio.projectknio.user.ApplicationUser;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "ratings")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "rating")
    private Integer rating;
    @Column(name = "comment", length = 1024)
    private String comment;

    @ManyToOne
    @JoinColumn(name="user_id")
    private ApplicationUser user;

    @ManyToOne
    @JoinColumn(name="place_id")
    private Place place;
}
