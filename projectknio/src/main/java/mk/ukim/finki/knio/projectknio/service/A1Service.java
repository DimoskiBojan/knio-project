package mk.ukim.finki.knio.projectknio.service;

import mk.ukim.finki.knio.projectknio.model.A1;

public interface A1Service {
    A1 parseEmail(String content);
}
