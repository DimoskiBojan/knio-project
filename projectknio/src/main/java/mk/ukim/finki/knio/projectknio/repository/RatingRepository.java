package mk.ukim.finki.knio.projectknio.repository;

import mk.ukim.finki.knio.projectknio.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Integer> {
    List<Rating> findByPlace_IdEquals(Integer id);
}
