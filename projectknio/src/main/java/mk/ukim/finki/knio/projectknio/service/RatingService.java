package mk.ukim.finki.knio.projectknio.service;

import mk.ukim.finki.knio.projectknio.model.Rating;

import java.util.List;

public interface RatingService {
    List<Rating> getPlaceRatings(Integer placeId);

    Rating addPlaceRating(Integer placeId, String user, Rating rating);
}
